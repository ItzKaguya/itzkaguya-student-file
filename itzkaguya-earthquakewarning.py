import requests
import json
import time

# URL endpoint API BMKG untuk data gempa bumi
url = 'https://data.bmkg.go.id/DataMKG/TEWS/autogempa.json'

# Ambang batas magnitudo gempa yang akan menjadi peringatan
threshold_magnitudo = 5.0

# Loop tak terbatas untuk menjalankan program
while True:
    try:
        # Mengirim permintaan HTTP GET untuk mengambil data gempa bumi
        response = requests.get(url)

        # Memeriksa apakah permintaan berhasil (status kode 200)
        if response.status_code == 200:
            # Mengambil data JSON dari respons
            data = json.loads(response.text)

            # Memeriksa apakah ada data gempa bumi
            if 'gempa' in data:
                # Mengambil data gempa terbaru
                gempa_terbaru = data['gempa'][0]

                # Mengambil informasi magnitudo, lokasi, dan waktu gempa
                magnitudo = float(gempa_terbaru['Magnitude'])
                lokasi = gempa_terbaru['Wilayah']
                waktu = gempa_terbaru['Tanggal'] + ' ' + gempa_terbaru['Jam']

                print('Gempa bumi terbaru di ' + lokasi + ' dengan magnitudo ' + str(magnitudo) + ' pada tanggal ' + waktu)

                # Memeriksa apakah magnitudo gempa melebihi ambang batas
                if magnitudo >= threshold_magnitudo:
                    print('PERINGATAN: Magnitudo gempa melebihi ambang batas!')
            else:
                print('Tidak ada data gempa bumi.')
        else:
            print('Gagal mengambil data gempa bumi. Kode status: ' + str(response.status_code))
    except Exception as e:
        print('Terjadi kesalahan: ' + str(e))

    # Menunggu 1 detik sebelum mengirim permintaan lagi
    time.sleep(1)
